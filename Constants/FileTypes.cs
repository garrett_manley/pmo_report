namespace PMO.Report.Constants
{
	public enum FileTypes
	{
		Csv
	}

	public static class FileTypesExtensions
	{
		public static string getExt(this FileTypes type)
		{
			switch (type)
			{
				case FileTypes.Csv:
					return ".csv";
				default:
					return string.Empty;
			}
		}
	}
}