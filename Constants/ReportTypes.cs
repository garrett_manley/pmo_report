using System;
using System.Collections.Generic;
using PMO.Report.Helpers;
using PMO.Report.Models.JiraIssue;

namespace PMO.Report.Constants
{
	public enum ReportTypes
	{
		GMF_Jira_Defect_Report
	}

	public static class ReportTypesExtensions
	{
		public static string RunReport(this ReportTypes type, string dataPath)
		{
			switch (type)
			{
				case ReportTypes.GMF_Jira_Defect_Report:
					return new FileHelpers.CsvHelper<JiraIssue>().ReadCsv(dataPath).WriteToGMFJiraDefectReport();
				default:
					throw new Exception("No report runner configured for this report type.");
			}
		}

		public static Models.Report GetReport(this ReportTypes type)
		{
			switch (type)
			{
				case ReportTypes.GMF_Jira_Defect_Report:
					return new Models.Report
					{
						ReportType = type,
						Alias = "gmf-jira-defects",
					};
				default:
					throw new Exception("Report Type not found.");
			}
		}

		public static List<string> GetReportList()
		{
			var reportTypesEnumValues = EnumHelpers.GetValues<ReportTypes>();
			var reports = new List<string>();

			foreach (var reportType in reportTypesEnumValues)
			{
				var report = reportType.GetReport();
				reports.Add(report.Alias);
			}

			return reports;
		}

		public static string GetReportListString()
		{
			return string.Join("\n\t", GetReportList());
		}
	}
}