using System;
using System.Collections.Generic;
using PMO.Report.Models;

namespace PMO.Report.Helpers
{
	public static class ConsoleHelpers
	{
		public static bool PromptForConfirmation(ConfirmationQuestion confirmationQuestion)
		{
			ConsoleKey response;
			do
			{
				Console.Write(confirmationQuestion.Question);
				response = Console.ReadKey(false).Key;
				if (response != ConsoleKey.Enter)
				{
					Console.WriteLine();
				}
			} while (response != ConsoleKey.Y && response != ConsoleKey.N);

			if (response == ConsoleKey.Y)
			{
				confirmationQuestion.Callback?.Invoke();
				return true;
			}
			else
			{
				Console.WriteLine("\n\nExiting...");
				return false;
			}

		}

		public static string PromptForMultiLineInput(string prompt)
		{
			string input;

			var endInputValue = "EXIT";
			var lines = new List<string>();

			Console.Write($"\nEnter \"");
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.Write(prompt);
			Console.ResetColor();
			Console.Write("\" info for this report (type ");
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Write(endInputValue);
			Console.ResetColor();
			Console.Write(" to terminate):\n\n");
			Console.ForegroundColor = ConsoleColor.Yellow;

			do
			{
				input = Console.ReadLine();

				if (input != endInputValue)
				{
					lines.Add(input);
				}
			} while (input != endInputValue);

			Console.ResetColor();

			var output = string.Join(Environment.NewLine, lines);

			return output;
		}

		public static void WriteError(string errorMessage)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.Error.WriteLine($"\n[Error]\t{errorMessage}");
			Console.ResetColor();
		}

		public static void WriteInfo(string infoMessage)
		{
			Console.ForegroundColor = ConsoleColor.Blue;
			Console.WriteLine("\n[INFO]\n");
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine(infoMessage);
			Console.ResetColor();
		}
	}
}