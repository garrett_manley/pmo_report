using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Markdig;
using PMO.Report.Constants;
using PMO.Report.Models;

namespace PMO.Report.Helpers
{
	public static class FileHelpers
	{
		public static List<string> GetSupportedFileTypes()
		{
			var typesEnumValues = EnumHelpers.GetValues<FileTypes>();
			var types = new List<string>();
			foreach (var type in typesEnumValues)
			{
				types.Add(type.getExt());
			}
			return types;
		}

		public static void WriteFile(string path, string fileName, string data)
		{
			var mdFile = path + fileName;

			File.WriteAllLines(mdFile,
			new List<string>(Regex.Split(data, Environment.NewLine)));
			Console.WriteLine($"\nGenerated Report:\t{mdFile}");
		}

		public static string ConvertMarkdownToHtml(string markdown)
		{
			var pipeline = new MarkdownPipelineBuilder().UseAdvancedExtensions().Build();
			return Markdown.ToHtml(markdown, pipeline);
		}

		public class CsvHelper<CsvObject> where CsvObject : IBaseCsvObject, new()
		{
			public CsvHelper() { }

			public IEnumerable<CsvObject> ReadCsv(string path)
			{

				Console.WriteLine($"\nReading data from {Path.GetFileName(path)}...");

				string[] lines;

				try
				{
					lines = File.ReadAllLines(path);
				}
				catch (IOException ex)
				{
					ConsoleHelpers.WriteError($"Could not read file:\n\n{ex.Message}");
					throw ex;
				}

				var parsedData = from line in lines
								 let data = line.Split(',')
								 select new CsvObject { Data = data };

				return parsedData;
			}
		}
	}
}