using System.Collections.Generic;

namespace PMO.Report.Models
{
	public interface IBaseCsvObject
	{
		string[] Data { get; set; }
	}

	public class BaseCsvObject : IBaseCsvObject
	{
		public string[] Data { get; set; }

		public BaseCsvObject() { }

		public BaseCsvObject(string[] data)
		{
			Data = data;
		}
	}
}