using System;

namespace PMO.Report.Models
{
	interface IConfirmationQuestion
	{
		string Question { get; set; }
		Action Callback { get; set; }
	}

	public class ConfirmationQuestion : IConfirmationQuestion
	{
		public string Question { get; set; }
		public Action Callback { get; set; }
	}
}
