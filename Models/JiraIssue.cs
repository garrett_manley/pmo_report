using System;
using System.Collections.Generic;
using System.Linq;
using PMO.Report.Helpers;

namespace PMO.Report.Models.JiraIssue
{
	public interface IJiraIssue
	{
		string IssueType { get; }
		string Key { get; }
		string ID { get; }
		string Summary { get; }
		string Assignee { get; }
		string Reporter { get; }
		string Priority { get; }
		string Status { get; }
		string Resolution { get; }
		string Created { get; }
		string Updated { get; }
		string DueDate { get; }
	}

	public class JiraIssue : BaseCsvObject, IJiraIssue
	{
		public string IssueType => Data[0];
		public string Key => Data[1];
		public string ID => Data[2];
		public string Summary => Data[3];
		public string Assignee => Data[4];
		public string Reporter => Data[5];
		public string Priority => Data[6];
		public string Status => Data[7];
		public string Resolution => Data[8];
		public string Created => Data[9];
		public string Updated => Data[10];
		public string DueDate => Data[11];
	}

	public static class JiraIssueExtensions
	{
		public static string WriteToGMFJiraDefectReport(this IEnumerable<JiraIssue> parsedData)
		{
			Console.WriteLine("\nParsing Jira data for defects metrics...");

			var defects = parsedData.Where(issue => issue.IssueType == "Defect");
			var open = defects.Where(issue => issue.Status == "Awaiting Approval" || issue.Status == "Ready For Development	");
			var moreInfo = defects.Where(issue => issue.Status == "More Information Needed");
			var InDev = defects.Where(issue => issue.Status == "In Development" || issue.Status == "Staged for Build");
			var testReady = defects.Where(issue => issue.Status == "Ready for QA");
			var InTest = defects.Where(issue => issue.Status == "In Testing");
			var done = defects.Where(issue => issue.Status == "Done");

			// TODO:: https://trello.com/c/TbBp6uYv - Extract "status" "accomplishments" and "opportunity" into shared methods to be used with other reports
			var status = ConsoleHelpers.PromptForMultiLineInput("Status");
			var accomplishments = ConsoleHelpers.PromptForMultiLineInput("Accomplishments");
			var opportunity = ConsoleHelpers.PromptForMultiLineInput("Potential Areas of Opportunity");

			var report = $@"# GM Financial Mobile

{status}

## Project Metrics

### Defects

#### Defect status in workflow

| Open | More Info Needed | In Development | Ready for Testing | In Testing | Done |
| :---: | :--------------: | :------------: | :---------------: | :--------: | :---: |
| `{open.Count()}` | `{moreInfo.Count()}` | `{InDev.Count()}` | `{testReady.Count()}` | `{InTest.Count()}` | `{done.Count()}` |

## Accomplishments

{accomplishments}

## Milestones

### Initial Launch of Mobile Platforms (Complete)

| Channel | UAT       | Alpha     | Beta      | Launch    |
| ------- | --------- | --------- | --------- | --------- |
| iOS     | complete. | complete. | complete. | complete. |
| Android | complete. | complete. | complete. | complete. |

### Next Milestone TBD

## Potential Areas of Opportunity

{opportunity}

---

Reported by: Garrett Manley

Report Run Time: {DateTime.Now.ToLocalTime()}";

			return report;
		}
	}
}