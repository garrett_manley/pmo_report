using System;
using PMO.Report.Constants;

namespace PMO.Report.Models
{
	public interface IReport
	{
		ReportTypes ReportType { get; set; }
		DataTypes DataType { get; set; }
		string Alias { get; set; }
		string RunReport(string dataPath);
	}

	public class Report : IReport
	{
		public ReportTypes ReportType { get; set; }
		public DataTypes DataType { get; set; }

		public string Alias { get; set; }

		public string RunReport(string dataPath)
		{
			return ReportType.RunReport(dataPath);
		}
	}
}