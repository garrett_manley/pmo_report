﻿using System;
using System.Collections.Generic;
using System.IO;
using PMO.Report.Constants;
using PMO.Report.Helpers;
using PMO.Report.Models;

namespace PMO.Report
{
	class Program
	{
		// args
		private static string _outputPath;
		private static string _dataFilePath;
		private static string _reportType;

		// Data
		public static string _dataFileExt;

		public static Models.Report SelectedReport { get; set; }

		private static void Main(string[] args)
		{
			if (ValidateArgs(args))
			{
				ProcessDataFile();
			}
		}

		private static void ProcessDataFile()
		{
			try
			{
				WriteReportToOutputDir(SelectedReport.RunReport(_dataFilePath));
			}
			catch (Exception ex)
			{
				ConsoleHelpers.WriteError(ex.Message);
				return;
			}
		}

		private static void WriteReportToOutputDir(string report)
		{
			try
			{
				ConsoleHelpers.WriteInfo(report);

				var mdFile = $"/PMO Report | {DateTime.Now.ToLongDateString()}.md";
				FileHelpers.WriteFile(_outputPath, mdFile, report);

				var htmlFile = $"/PMO Report | {DateTime.Now.ToLongDateString()}.html";
				var convertedHtml = FileHelpers.ConvertMarkdownToHtml(report);
				FileHelpers.WriteFile(_outputPath, htmlFile, convertedHtml);
			}
			catch (Exception ex)
			{
				ConsoleHelpers.WriteError($"Could not write file:\n\n{ex.Message}");
				return;
			}
		}

		private static bool ValidateArgs(string[] args, int validNumArgs = 3)
		{
			if (args.Length > validNumArgs || args.Length < validNumArgs)
			{
				ShowHelp();
				return false;
			}

			_reportType = args[0];
			_dataFilePath = args[1];
			_outputPath = args[2];

			return ValidateReportType() && ValidateDataFilePath() && ValidateOutputPath();
		}

		// TODO:: https://trello.com/c/gBkROsY5 - Update this method name as it does more than validate.
		private static bool ValidateOutputPath()
		{
			_outputPath += $"/{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";

			if (Directory.Exists(_outputPath))
			{
				var question = new ConfirmationQuestion
				{
					Question = $"\nThe directory {_outputPath} already exists, would you like to overwrite it? [y/n] ",
					Callback = CreateOutputDirectory
				};

				return ConsoleHelpers.PromptForConfirmation(question);
			}
			else
			{
				CreateOutputDirectory();
				return true;
			}
		}

		private static bool ValidateDataFilePath()
		{
			if (!File.Exists(_dataFilePath))
			{
				var question = new ConfirmationQuestion
				{
					Question = $"The data file {_dataFilePath} does not exist, would you like to generate a blank template? [y/n] ",
					Callback = CreateBlankTemplate
				};
				ConsoleHelpers.PromptForConfirmation(question);

				return false;
			}
			else
			{
				var ext = Path.GetExtension(_dataFilePath);
				var fileName = Path.GetFileName(_dataFilePath);

				if (ext == null || ext == String.Empty)
				{
					Console.WriteLine($"The file {fileName} does not specify a file extension.\nUnable to parse the data without this information.\n\nExiting...");
					return false;
				}
				else
				{
					var isFileTypeSupported = false;

					foreach (var extension in FileHelpers.GetSupportedFileTypes())
					{
						if (ext == extension)
						{
							isFileTypeSupported = true;
							break;
						}
					}

					if (!isFileTypeSupported)
					{
						Console.WriteLine($"The file extension {ext} is not currently supported.\nUnable to parse the provided data.\n\nExiting...");
						return isFileTypeSupported;
					}
					else
					{
						_dataFileExt = ext;
						return isFileTypeSupported;
					}
				}
			}
		}

		private static bool ValidateReportType()
		{
			var reportTypesEnumValues = EnumHelpers.GetValues<ReportTypes>();
			var reportTypes = new List<Models.Report>();
			try
			{
				foreach (var reportType in reportTypesEnumValues)
				{
					var report = reportType.GetReport();
					if (report.Alias == _reportType)
					{
						SelectedReport = report;
						return true;
					}
				}

				throw new Exception($"The specified report alias \"{_reportType}\" could not be found.");
			}
			catch (Exception ex)
			{
				ConsoleHelpers.WriteError($"Could not validate report type:\n\n{ex.Message}");
				ShowHelp();
			}

			return false;
		}

		private static void CreateBlankTemplate()
		{
			Console.WriteLine("Template generation not implemented yet.\n\nExiting...");
		}

		private static void CreateOutputDirectory()
		{
			Directory.CreateDirectory(_outputPath);
		}

		private static void ShowHelp() => Console.WriteLine($@"
PMO Report Generator:

	Help
	_____________________________
	|                           |
	|  Usage:                   |
	|                           |
	|    Args:                  |
	|                           |	
	|      0: Report Alias      |
	|      1: Data File Path    |
	|      2: Output File Path  |
	|                           |
	=============================

Available Reports:

	{ReportTypesExtensions.GetReportListString()}
");
	}
}
