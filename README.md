.NET Core Console application used to generate the markdown needed to write a PMO report. The application also supplies utilities for parsing markdown to HTML as well as automatic data processing to generate metrics.

The plan is to allow this program the be extensible and used by other LDC employees to make report generation easier and more timely.
